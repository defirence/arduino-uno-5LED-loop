# arduino-uno-5LED-loop

### Description:
A Sketchbook based on a YouTube tutorial, lights up and loops 5 LED's in sequence.

### Requirements:
- Arduino Uno
- USB Cable
- Arduino IDE
- Some C++ knowledge (If you want to modify or hack this)
- 5 x 200Ω Resistors
- 5 x Green Jumper Cables
- 1 x Breadboard
- 1 x Black Jumper Cable (Ground)
- 5 x LED's (For this I used: Green, Red, Yellow, Blue + One RGB Diode)

### Instructions:
- Open the .ido file in your Arduino IDE, Verify and then Upload it.
- Press Reset if required to clear it and run the new code.
- Enjoy your blinking colorful LED's! :)

Original YouTube Video: `/watch?v=e1FVSpkw6q4`
